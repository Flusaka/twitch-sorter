import TwitchAPI from './twitch-api';

class TwitchHelper {
    constructor() {
        TwitchAPI.clientID = 'ef7k0s2mkb7h9xebqd575tfza38wvu';
    }

    refresh(username, success, failure) {
        TwitchAPI.users.usersByName({
            users: username
        }, (error, response) => {
            if(!error) {
                TwitchAPI.users.follows({
                    userID: response.users[0]._id,
                    limit: 50,
                    sortby: 'last_broadcast'
                }, (error, response) => {
                    if(!error) {
                        if(response.follows.length > 0) {
                            const channels = response.follows.map((followLink) => {
                                const channel = followLink.channel;

                                return {
                                    id: channel._id,
                                    display_name: channel.display_name,
                                    url: channel.url,
                                    logo: channel.logo
                                };
                            }),
                            channelIDs = channels.map((channel) => {
                                return channel.id.toString();
                            }).join(',');

                            TwitchAPI.streams.live({
                                channel: channelIDs,
                                limit: channels.length
                            }, (error, response) => {
                                if(!error) {
                                    response.streams.forEach((stream) => {
                                        const foundChannel = channels.find((channel) => {
                                            return channel.id === stream.channel._id.toString();
                                        });

                                        foundChannel.stream = {
                                            game: stream.game,
                                            vodcast: stream.stream_type === 'watch_party' || stream.stream_type === 'rerun',
                                            viewers: stream.viewers,
                                            created_at: stream.created_at
                                        };
                                    });

                                    let videoRetrieveCount = 0;

                                    channels.forEach((channel) => {
                                        TwitchAPI.channels.videos({
                                            channelID: channel.id,
                                            limit: 1,
                                            broadcast_type: 'archive'
                                        }, (error, response) => {
                                            if(!error) {
                                                if(response.videos.length > 0) {
                                                    const video = response.videos[0];
                                                    channel.video = {
                                                        url: video.url,
                                                        created_at: video.created_at,
                                                        published_at: video.published_at,
                                                        length: video.length,
                                                        game: video.game
                                                    };
                                                }

                                                if(++videoRetrieveCount >= channels.length) {
                                                    channels.sort(function(a, b) {
                                                        if(a.stream && !b.stream) {
                                                            return -1;
                                                        }
                                                        else if(!a.stream && b.stream) {
                                                            return 1;
                                                        }
                                                        else if(a.video && !b.video) {
                                                            return -1;
                                                        }
                                                        else if(!a.video && b.video) {
                                                            return 1;
                                                        }
                                                        else if(a.video && b.video) {
                                                            var firstDate = new Date(a.video.published_at),
                                                                secondDate = new Date(b.video.published_at);
                
                                                            return secondDate - firstDate;
                                                        }
                                        
                                                        return a.display_name.localeCompare(b.display_name);                
                                                    });

                                                    success(channels);
                                                }
                                            }
                                            else {
                                                // TODO: Perhaps, if it fails, we need to stop this loop?
                                                // May refactor to be a normal loop and break out
                                                failure(error);
                                            }
                                        });
                                    });
                                }
                                else {
                                    failure(error);
                                }
                            });
                        }
                        else {
                            failure('You don\'t follow any channels!');
                        }
                    }
                    else {
                        failure(error);
                    }
                });
            }
            else {
                failure(error);
            }
        });
    }
}

export default TwitchHelper;