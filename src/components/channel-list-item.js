import React from 'react';

function ChannelListItem(props) {
    function getTimeSinceLastStream(date) {
        var now = Date.now(),
            lastStream = new Date(date),
            diff = (now - lastStream) / 1000,
            days = Math.round(diff / (3600 * 24)),
            hours = Math.round(diff / 3600);

        if(days > 0) {
            if(days > 28) {
                return '> a month ago'
            }
            return days + (days === 1 ? ' day ago' : ' days ago');
        }
        else if(hours > 0) {
            return hours + (hours === 1 ? ' hour ago' : ' hours ago');
        }
        
        return '< 1 hour ago';
    }

    function getVideoLength(time) {
        var days = Math.floor(time / (3600 * 24));
        time -= days * 3600 * 24;
        var hrs = Math.floor(time / 3600);
        time -= hrs*3600;
        var mnts = Math.floor(time / 60);
        time -= mnts*60;

        var lengthString = '';
        if(days > 0) {
            lengthString += days + 'd ';
        }
        if(hrs > 0) {
            lengthString += hrs + 'h ';
        }
        if(mnts > 0) {
            lengthString += mnts + 'm ';
        }
        lengthString += time + 's';
        return lengthString;
    }

    const   channel = props.channel,
            gamePlaying = channel.stream ? channel.stream.game : (channel.video ? channel.video.game : '');        
    
    return (<li>
        <a className='channel-logo' href={channel.url} target='_blank'>
            <div className='logo'>
                {channel.stream &&
                    <div className={`live ${channel.stream.vodcast ? ' vodcast' : ''}`}></div>
                }
                <img src={channel.logo} alt={channel.display_name} />
            </div>
        </a>
        <div className='channel-info'>
            <div className='channel-name'>{channel.display_name}</div>
            <div className='game'>{gamePlaying}</div>
        </div>
        {channel.video &&
            <div className='video'>
                <div className='video-info'>
                    <div className='video-date'>{getTimeSinceLastStream(channel.video.published_at)}</div>
                    <div className='video-length'>{getVideoLength(channel.video.length)}</div>
                </div>
                <a href={channel.video.url} target='_blank'><div className='link'></div></a>
            </div>
        }
    </li>)
}

export default ChannelListItem;