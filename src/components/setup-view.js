import React from 'react';

function SetupView(props) {
    return (
        <div>
            <input type='text' value={props.username} onChange={props.onUsernameChanged} /><br/>
            <input type='submit' value='Retrieve Channels' onClick={props.onSetupSubmit} />
        </div>
    )
}

export default SetupView;