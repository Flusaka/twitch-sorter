import React from 'react';
import PropTypes from 'prop-types';
import ChannelListItem from './channel-list-item';

function ChannelList(props) {
    const listItems = props.channels.map((channel, index) => {
        return <ChannelListItem key={channel.id} channel={channel} />
    });
    return (
        <ul>{listItems}</ul>
    )
}

ChannelList.propTypes = {
    channels: PropTypes.array.isRequired
}

export default ChannelList;