import React, { Component } from 'react';
import TwitchHelper from '../util/twitch-helper';
import SetupView from './setup-view';
import ChannelList from './channel-list';

class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      state: 'setup',
      username: '',
      channels: []
    };

    this.TwitchHelper = new TwitchHelper();

    this.refresh = this.refresh.bind(this);
    this.submitSetup = this.submitSetup.bind(this);
    this.usernameChanged = this.usernameChanged.bind(this);
  }

  componentDidMount() {
    // TODO: Check if username is stored and refresh
    //this.refresh();
  }

  refresh() {
    this.TwitchHelper.refresh(this.state.username, (channels) => {
        this.setState({
          channels: channels,
          state: 'channels'
        });
      }, (error) => {
        console.log(error);
      });
  }

  submitSetup(event) {
    this.refresh();
    event.preventDefault();
  }

  usernameChanged(event) {
    this.setState({
      username: event.target.value
    });
  }

  render() {
    return (
      <div className='main-content'>
        {
          this.state.state === 'setup' && (
            <SetupView onSetupSubmit={this.submitSetup} onUsernameChanged={this.usernameChanged} username={this.state.username} />
          )
        }
        {
          this.state.state === 'channels' && (
            <div>
              <button onClick={this.refresh}>Refresh</button>
              <ChannelList channels={this.state.channels}/>
            </div>
          )
        }
      </div>
    );
  }
}

export default Main;
